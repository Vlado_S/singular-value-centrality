# Singular Value Centrality


## Description
This code calculates the singular value centrality (CSV) as proposed here: [https://doi.org/10.1021/acs.jcim.4c00973](https://doi.org/10.1021/acs.jcim.4c00973)

It is a specialised case which we call the "contact" CSV, as it ranks vertices on subsystem interfaces. This implementation was made with focus for application to (bio)molecular complexes modelled by residue interaction network (RIN) models. The rank can account for effects of both direct and water-mediated contacts (interactions) between the protein residues. 

However, the concept and this code can be used in ANY other network scenario where the subsystems can be anything (e.g. railway, bus and taxi networks and study the interfaces between them).

## Installation
No special installation is necessary. Just make sure you have reasonably recent python3 (3.6 or newer), Matplotlib, NetworkX, Numpy. The modules in the zipped forlder `required_modules.zip` should be available for the main script, i.e. place them in some python-recognised directory, from where your python uses modules, or place them in the same directory, from which you run the main script.

## Usage
Read the manual.

For the impatient ones, in terminal run

`> python SVD_PPI_centrality.py param_inp.csvinp`

The file `param_inp.csvinp` contains the parameters controlling the execution (you can name your file however you want). They are described in the file itself. You can find the `.csvinp` files in the directories in `use_examples.zip`. There are two subdirectories - one for the case when you'd use it with the GAMESS FMO output (probably limited to chemistry applications) and one, where data is read from a general .csv type file. The latter is very useful if you want to apply this method to some general case/network.

There is an additional directory with examples `data_from_paper.zip`. This directory contains data specifically used in the paper where this method was introduced. These files can be used to reproduce the published values, pictures, etc.

## Support and/or Contributing
By email to corresponding author in the main paper (also owner of this GitLAB profile). Feel free to suggest changes, applications, collaborations...

## License
FOSS GPL3. But please, if you use it, acknowledge the author and the paper.
