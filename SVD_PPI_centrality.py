__author__    = "Vladimir Sladek"
__email__     = "sladek.vladimir@savba.sk"
__copyright__ = """ Copyright since 2024

                    This is free software: you can redistribute it and/or modify
                    it under the terms of the GNU General Public License as published by
                    the Free Software Foundation, either version 3 of the License, or
                    (at your option) any later version.
                    
                    This code is distributed in the hope that it will be useful,
                    but WITHOUT ANY WARRANTY; without even the implied warranty of
                    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
                    GNU General Public License for more details.
                    
                    GNU General Public License: <https://www.gnu.org/licenses/>.
                """



#-----------------------------------
#    G L O B A L  I M P O R T S
#-----------------------------------



import numpy as np
#import networkx as nx
import time
import argparse
import sys
#print(sys.path)
#print(sys.executable)



#-----------------------------------
#       F U N C T I O N S
#-----------------------------------

def print_header(file):
    '''
    Write header to output file.

    Parameters
    ----------
    file : file, must be opened
    '''
    
    file.write(''' Singular Value Centrality (CSV) calculation
               
 author: Vladimir Sladek (sladek.vladimir@savba.sk)
 source: https://gitlab.com/Vlado_S/singular-value-centrality
 theory: ...
 license: GNU General Public License, https://www.gnu.org/licenses/
     
 ''')
    
    
    file.write('''Comments:
 i: Node ID
 subs: Subsystem ID (a.k.a. SSid)
 wg_sum: Sum of weights of given node in the subsystem interaction interface.
         When waters are included, then also those weights are counted in.
         If pair interaction energies (PIE) are used for weights, then this
         sum equals the "total interaction energy" (TIE).
 #wb: Number of water bridges given residue has.
      For water molecules we just print 0.
 C_SV: The centrality

 ''')
 
    
    file.write('\n --- R E S U L T S ---\n')
    return

#-----------------------------------

def topo_stat(A, node_centr):
    '''
    Do some degree statistics on A_kl.

    Parameters
    ----------
    A : numpy array (matrix)
        The adjacency matrix block (weighted).
        Assuming not symmetrical, but edges in
        the subsystem interface.
    '''
       
    # Assume rows correspond to vertices in subsystem 1
    # and columns correspond to subsystem 2.
    dgr_w = []
    dgr = []
    
    n1, n2 = A.shape
    
    # subsystem 1 (rows)
    for i in range(n1):
        tmp = A[i,:]
        dgr_w.append(sum(tmp))
        dgr.append(np.count_nonzero(tmp))
        
    
    # subsystem 2 (columns)
    for j in range(n2):
        tmp = A[:,j]
        dgr_w.append(sum(tmp))
        dgr.append(np.count_nonzero(tmp))
    
    # Collect results (append to (if) existing file)
    out1 = open('degree_stat_out.dat', 'a')
    out2 = open('edge_stat_out.dat', 'a')
    
    for i in range(n1+n2):
        out1.write('{0} {1} {2}\n'.format(dgr[i], dgr_w[i], node_centr[i]))
    
    out1.close()
    out2.close()
    
    return

#-----------------------------------

def symmCSV(A):
    '''
    Calculate S(ingular) V(alue) Centrality
    for the case when A is symetric. So use
    only U matrix.
    
    Parameters
    ----------
    A : np.array (2d)
        The matrix on which SVD will be done.

    Returns
    -------
    C_SV : list
        Contains CSV values for nodes (first nA - rows + next nB - columns in A)
    '''
    
    # SVD
    U, S, VT = np.linalg.svd(A, full_matrices = False)
    
    k = S.shape[0]
    n = A.shape[0]
    
    # The tilde matrix tU (tV not needed in symmetric case)
    # and the SV centrality.

    C_SV = []
    
    #tU = np.zeros((n, k))
    
    for i in range(n):
        tu_i = [np.sqrt(S[j])*U[i,j] for j in range(k)]
        #print(i, tu_i)
        
        #tU[i,:] = tu_i
        
        C_SV.append(np.sqrt(sum(v**2 for v in tu_i)))
    
    # Just for test
    '''
    for i in range(n):
        tv_i = [np.sqrt(S[j])*VT[j,i] for j in range(k)]
        #print(i, tv_i)
        
        #tV[:,i] = tv_i
        
        C_SV.append(np.sqrt(sum(v**2 for v in tv_i)))
    '''

    return C_SV

#-----------------------------------
def asymmCSV(A):
    '''
    Calculate S(ingular) V(alue) Centrality
    for the case when A is assymetric. So use
    both U and V matrices.
    
    Parameters
    ----------
    A : np.array (2d)
        The matrix on which SVD will be done.

    Returns
    -------
    C_SV : list
        Contains CSV values for nodes (first nA - rows + next nB - columns in A)
    '''
    
    # SVD
    # np.linalg.svd uses LAPACK routine _gesdd:  https://numpy.org/doc/stable/reference/generated/numpy.linalg.svd.html
    # which in turn does divide and conquer SVD:  https://netlib.org/lapack/explore-html/df/d22/group__gesdd_ga8941e5ff50de36580dae8940015e9cb0.html
    U, S, VT = np.linalg.svd(A, full_matrices = False)
    
    k = S.shape[0]
    nA, nB = A.shape[0], A.shape[1]
    
    # The tilde matrices tU, tV
    # and the SV centrality

    C_SV = []
    
    tU = np.zeros((nA, k))
    tV = np.zeros((k, nB))

    for i in range(nA):
        #tu_i = U[i,:]
        tu_i = [np.sqrt(S[j])*U[i,j] for j in range(k)]
        #print(i, tu_i)
        
        tU[i,:] = tu_i
        
        C_SV.append(np.sqrt(sum(v**2 for v in tu_i)))

    for i in range(nB):
        #tv_i = VT[:,i]
        tv_i = [np.sqrt(S[j])*VT[j,i] for j in range(k)]
        #print(i, tv_i)
        
        tV[:,i] = tv_i
        
        C_SV.append(np.sqrt(sum(v**2 for v in tv_i)))
    
    return C_SV

#-----------------------------------

def draw_bar_plot_clickable(x_lbls, y_values, color=None, fig_name=None, border=None, quartiles=None, show_click_plot=False, ylabel=None):
    '''
    Draw a clickable bar plot.
    
    Parameters
    ----------
    x_lbls : list
        The labels of the vertices for which bars will be plotted
    
    y_values : list
        The values to be plotted / heights of the bars
        (len(x_lbls) == len(y_values))
    
    color : list or str or None
        list : the color(s) of the bars for each x_lbl
        str :  one value of matplotlib recognised colour
        None : defalt (blue) colour will be used
    
    fig_name : str
        name of the saved figure
    
    border : int/float or list/tuple or None
        float : x-position(s) where a marker will be placed to indicate
                the border between subsystem 1 and 2 (and 3 ...)
        None : no marker
    
    quartiles : None / full / nonzero
        None : no quartiles will be drawn
        full : quartiles for all (y) values will be calculated (incl. zeros)
        nonzero : quartiles for only nonzero y will be calculated
    
    show_click_plot : True / Flase
        show (or not) the bar plot in a matplotlib window

    Returns
    -------
    nothing, but draws the plot
    '''
    
    import matplotlib.pyplot as plt
    
    fig, ax = plt.subplots(figsize=(7, 5))
    
    
    # Quartiles
    if quartiles != None:
        if quartiles == 'nonzero':
            tmp = [v for v in y_values if v > 0.0001]
        else:
            tmp = y_values
            
        q1, q2, q3 = np.nanquantile(tmp, [0.25, 0.5, 0.75])
        IQR = q3 - q1                        # interquartile range
        l_o_b = q1 - 1.5 * IQR               # lower outlier boundary
        u_o_b = q3 + 1.5 * IQR               # upper outlier boundary
        
        print('Quartiles ("', quartiles, '"):\n\tQ1 =', q1,'\n\tQ2 =', q2, '\n\tQ3 =', q3, '\n\tmin, max =', np.nanmin(y_values), ',', round(np.nanmax(y_values), 4))
        
        # draw q
        ax.axhspan(np.nanmin(y_values), q1, color = 'lavender', alpha = 0.5)
        ax.axhspan(q1, q2,  color = 'oldlace', alpha = 0.5)
        ax.axhspan(q2, q3,  color = 'lavender', alpha = 0.5)
        ax.axhspan(q3, np.nanmax(y_values), color = 'oldlace', alpha = 0.5)
        
        #ax.axhline(y = l_o_b, color="green", linestyle="-.", linewidth=True)
        #ax.axhline(y = u_o_b, color="green", linestyle="-.", linewidth=True)
        ax.axhline(y = q2, color="darkorange", linestyle="-.", linewidth=True)
        
    
    # Check if color set
    x = [i for i in range(len(x_lbls))]
    if color == None:
        color = 'blue'
    
    
    # Draw bar plot
    plt.bar(x, y_values, tick_label=x_lbls, color=color, picker=True)
    plt.setp(ax.get_xticklabels(), rotation=90, ha="right", rotation_mode="anchor")
    
    
    # Ad border marker on x axis
    if border != None:
        # Check if list or float
        if isinstance(border, float) or isinstance(border, int):
            # otherwise assume list/tuple
            border = [border]
        
        for b in border:
            ax.plot(b-0.5, 0, marker = 7, markerfacecolor='black', markersize=10)
    
    
    # If many bars, do not show all x-labels
    #plt.setp(ax.get_xticklabels()[::5], visible=False)
    l = len(x)
    borders = {1: 1, 20 : 2, 50 : 4, 100 : 6, 150 : 10, 200 : 20,  500 : 40, 1000 : 60}
    for k, v in borders.items():
        if l >= k:
            n = v
    
    ax.set_xticks(x[::n])
    ax.set_xticklabels(x_lbls[::n])
    
    
    # y-axis title
    if ylabel != None:
        #ylabel = r'$C^{\mathrm{SV}}$'
        ax.set_ylabel(ylabel)
    
    
    # Draw, show and save
    plt.tight_layout()
    if show_click_plot == True:
        
        # Connect picker
        #fig.canvas.callbacks.connect('button_press_event', lambda event : on_pick_bar(event, x_lbls))
        fig.canvas.callbacks.connect('pick_event', lambda event : on_pick_bar(event, x_lbls))
        
        # Show in matplotlib window
        plt.tight_layout()
        plt.show()
    
    if fig_name == None:
        fig_name = 'bar_plot.png'
    fig.savefig(fig_name, dpi=300)
    
    return

#-----------------------------------

def on_pick_bar(event, txt_lbls):
    # If we use center positioning of the labels, then the x position of each bar 
    # is the position of its' left side and each bar occupies a total space (horizontal width) l = 1. 
    # Each bar has a (visible) width w (by default 0.8 * l). The x can be obtained by .get_x() and 
    # the width by .get_width(). For the n_th bar, its' x position will be: x_n = n - w/2 
    # (since l = 1, we do not need to write n*l - w/2).
    # Hence, the order numeral (index) of each bar can be obtained as n = x_n + w/2.

    # https://www.youtube.com/watch?v=EJ1V6DwmabU

    this_bar = event.artist
    
    x = this_bar.get_x()
    w = this_bar.get_width()
    n = int(x + w / 2)
    
    h = this_bar.get_height()
    
    txt_lbl = txt_lbls[n]
    
    print('index: {0}, label: {1}, value: {2}'.format(n, txt_lbl, h))

#-----------------------------------

def get_PIC(i, j, E_es, G_solv, E_disp):
    '''
    PIC - Pair Interaction Character (as used by Alex Heifetz).
    
    ref: Alexander Heifetz, Vladimir Sladek, Andrea Townsend-Nicholson, Dmitri G. Fedorov; 
         Characterizing Protein-Protein Interactions with the Fragment Molecular Orbital Method 
         in Quantum Mechanics in Drug Discovery, Springer, 2020 
         Print ISBN: 978-1-0716-0281-2, Online ISBN: 978-1-0716-0282-9 
         https://doi.org/10.1007/978-1-0716-0282-9
    
    Parameters
    ----------
    i,j : int
        indices of residues (in the energy matrix) for which PIC is calculated
    E_es, G_solv, E_disp : np array
        matrices containing the respective energy terms

    Returns
    -------
    PIC_f : float
        PIC factor
    '''
    
    tmp = abs(E_es[i,j] + G_solv[i,j])
    tmp2 = tmp + abs(E_disp[i,j])
    if tmp2 != 0:
        PIC_f = tmp / tmp2
    else:
        PIC_f = None
    
    return PIC_f

#-----------------------------------

def get_PIE(fmo_out, NFRAG, SEGMENTS_USE):
    '''
    Read PIE (pair interaction energies) from GAMESS US output.
    The type of energy calculated determins types of PIEDA terms.

    Parameters
    ----------
    fmo_out : str
        The FMO GAMESS output file containing PIEDA energies.
    
    NFRAG : int
        Number of fragments/segments in the calculation.
    
    SEGMENTS_USE : bool
        If False, expect "normal" FMO calulation.
        If True, expect PA (partition analysis) and use these energies.

    Returns
    -------
    E_tot, E_es, E_disp, G_solv : numpy array (NFRAG x NGRAG)
        Arrays containing the PIEs and their components.
    '''
    
    import Read_FMO
    
    NREC = int(NFRAG*(NFRAG-1)/2)
    IFMO = Read_FMO.READ_IFMO(fmo_out)
    IPIEDA = Read_FMO.READ_IPIEDA(fmo_out)
    IFMO = Read_FMO.READ_IFMO(fmo_out)
    FMO_method = Read_FMO.FMO_METD(fmo_out)
    FMO_vers = Read_FMO.FMO_VERSION(fmo_out)
    if IFMO == 0:
        PCM = False
    else:
        PCM = True
    #print(NFRAG, NREC, IFMO)

    #_, _, _, _, E_tot, E_es, E_disp, G_solv, _ = Read_FMO.FMO_SEG_PIE(fmo_out, NFRAG, NREC, PCM=True, IFMO=IFMO)
    
    if IPIEDA == 0:
        
        if SEGMENTS_USE == True:
            iif, jif, iief, jief, E_tot, E_es, E_disp, G_solv, R_vdW = Read_FMO.FMO_SEG_PIE(fmo_out, NFRAG, NREC, PCM, IFMO)
        else:
            iif, jif, iief, jief, E_tot, G_solv, R_vdW, dEIJ = Read_FMO.FMO_PIE(fmo_out, NFRAG, NREC, PCM, IFMO)
    
    if IPIEDA == 1:
        
        kwargs = {}
        kwargs['FMO_method'] = FMO_method
        kwargs['FMO_vers'] = FMO_vers
        
        if 'MP2' in FMO_method:
            iif, jif, iief, jief, R_vdW, E_tot, E_es, E_ex, E_ctmx, E_rcdi, G_solv, _ = Read_FMO.FMO_PIE_PIEDA(fmo_out, NFRAG, NREC, PCM, IFMO, **kwargs)
            #PIEDA_terms = [E_es, E_ex, E_ctmx, E_rcdi, G_solv]
            #PIEDA_term_names = ['E_es', 'E_ex', 'E_ctmx', 'E_rcdi', 'G_solv']
        
        if 'DFTB' in FMO_method:
            
            if SEGMENTS_USE == True:
                iif, jif, iief, jief, E_tot, E_es, E_disp, G_solv, R_vdW = Read_FMO.FMO_SEG_PIE(fmo_out, NFRAG, NREC, PCM, IFMO)
                #PIEDA_terms = [E_es, E_disp, G_solv]
                #PIEDA_term_names = ['E_es', 'E_disp', 'G_solv']
            else:
                iif, jif, iief, jief, R_vdW, E_tot, E_es, E_0, E_ctes, E_disp, G_solv, _ = Read_FMO.FMO_PIE_PIEDA(fmo_out, NFRAG, NREC, PCM, IFMO, **kwargs)
                #PIEDA_terms = [E_es, E_0, E_ctes, E_disp, G_solv]
                #PIEDA_term_names = ['E_es', 'E_0', 'E_ctes', 'E_disp', 'G_solv']
        
        if 'HF3c' in FMO_method:
            iif, jif, iief, jief, R_vdW, E_tot, E_es, E_ex, E_ctmx, E_bs, E_disp, G_solv = Read_FMO.FMO_PIE_PIEDA(fmo_out, NFRAG, NREC, PCM, IFMO, **kwargs)
            #PIEDA_terms = [E_es, E_ex, E_ctmx, E_bs, E_disp, G_solv]
            #PIEDA_term_names = ['E_es', 'E_ex', 'E_ctmx', 'E_bs', 'E_disp', 'G_solv']
        
        if 'DFT' in FMO_method and 'DFTB' not in FMO_method:
            if FMO_vers < 5.4:
                iif, jif, iief, jief, R_vdW, E_tot, E_es, E_ex, E_ctmx, E_disp, G_solv, _ = Read_FMO.FMO_PIE_PIEDA(fmo_out, NFRAG, NREC, PCM, IFMO, **kwargs)
                #PIEDA_terms = [E_es, E_ex, E_ctmx, E_disp, G_solv]
                #PIEDA_term_names = ['E_es', 'E_ex', 'E_ctmx', 'E_disp', 'G_solv']
            else:
                iif, jif, iief, jief, R_vdW, E_tot, E_es, E_ex, E_ctmx, E_rc, E_disp, G_solv = Read_FMO.FMO_PIE_PIEDA(fmo_out, NFRAG, NREC, PCM, IFMO, **kwargs)
                #PIEDA_terms = [E_es, E_ex, E_ctmx, E_rc, E_disp, G_solv]
                #PIEDA_term_names = ['E_es', 'E_ex', 'E_ctmx', 'E_rc', 'E_disp', 'G_solv']
    
    return E_tot, E_es, E_disp, G_solv

#-----------------------------------

def MOLFRG_from_SS_dict(SS_dict):
    '''
    Compile MOLFRG from SS_dict information.

    Parameters
    ----------
    SS_dict : dictionary
        SS_dict is a dictionary, with 
        key = SSid (MOLFRG entry), 
        val = list of indices in MOLFRG to be changed to corresponding key

    Returns
    -------
    MOLFRG : list
        SSid for each node
    '''
    
    # The default is to set SSid = 1 for all nodes
    MOLFRG = [1 for _ in range(NFRAG)]
    
    for k, val in SS_dict.items():
        key = int(k)
        indlst = val.split(',')
        for ind in indlst:
            if '-' in ind:
                tmp = (ind.replace(' ','')).split('-')
                sind = int(tmp[0])
                eind = int(tmp[1])
                for i in range(sind, eind+1):
                    MOLFRG[i] = key
            else:
                MOLFRG[int(ind)] = key
    
    return MOLFRG

#-----------------------------------

def make_GPPI_kl(G, idk, idl, E_es, G_solv, E_disp):
    '''
    Construct an bipartite PPI graph betwee subsystems k,l.

    Parameters
    ----------
    G : networx graph
        The whole RIN.
        
    idk, idl : int
        IDs of the nodes belonging to subsystems k,l 
        (the monomer attribute of G nodes).
    
    E_es, G_solv, E_disp : numpy arrays (2d)
        The corresponding PIEs.
    
    Returns
    -------
    G_PPI_kl : networx graph
        The bipartite PPI graph betwee subsystems k,l.
    '''
    
    G_PPI_kl = nx.Graph()
    
    n = nx.number_of_nodes(G)
    
    for j in range(n):
        
        j_mono = G.nodes[j]['monomer']
        
        for i in range(j):
            
            if G.has_edge(i,j):
                
                i_mono = G.nodes[i]['monomer']
                
                if (i_mono == idk and j_mono == idl) or (i_mono == idl and j_mono == idk):
                
                    G_PPI_kl.add_node(i, monomer = G.nodes[i]['monomer'], label = G.nodes[i]['label'])
                    G_PPI_kl.add_node(j, monomer = G.nodes[j]['monomer'], label = G.nodes[j]['label'])
                    G_PPI_kl.add_edge(i, j, Ene = G[i][j]['weight'], weight = abs(G[i][j]['weight']), edge_label=round(G[i][j]['weight'],1), PIC = get_PIC(j, i, E_es, G_solv, E_disp))
    
    return G_PPI_kl

#-----------------------------------

def make_GPPI_klw(G, idk, idl, idw, E_es, G_solv, E_disp):
    '''
    Construct an tripartite PPI graph betwee subsystems k,l,w.

    Parameters
    ----------
    G : networx graph
        The whole RIN.
        
    idk, idl, idw : int
        IDs of the nodes belonging to subsystems k,l,w
        (the monomer attribute of G nodes).
    
    E_es, G_solv, E_disp : numpy arrays (2d)
        The corresponding PIEs.
    
    Returns
    -------
    G_PPI_klw : networx graph
        The bipartite PPI graph betwee subsystems k,l.
    '''
    
    G_PPI_klw = nx.Graph()
    
    n = nx.number_of_nodes(G)
    
    for j in range(n):
        
        j_mono = G.nodes[j]['monomer']
        
        for i in range(j):
            
            if G.has_edge(i,j):
                
                i_mono = G.nodes[i]['monomer']
                
                # k -- l, k -- w, l -- w edges
                if (i_mono == idk and j_mono == idl) \
                or (i_mono == idl and j_mono == idk) \
                or (i_mono == idk and j_mono == idw) \
                or (i_mono == idw and j_mono == idk) \
                or (i_mono == idl and j_mono == idw) \
                or (i_mono == idw and j_mono == idl):
                
                    G_PPI_klw.add_node(i, monomer = G.nodes[i]['monomer'], label = G.nodes[i]['label'])
                    G_PPI_klw.add_node(j, monomer = G.nodes[j]['monomer'], label = G.nodes[j]['label'])
                    G_PPI_klw.add_edge(i, j, Ene = G[i][j]['weight'], weight = abs(G[i][j]['weight']), edge_label=round(G[i][j]['weight'],1), PIC = get_PIC(j, i, E_es, G_solv, E_disp))
    
    return G_PPI_klw

#-----------------------------------

def make_Akl(G, k, l, dimk, diml, idk, idl):
    '''
    Extract the Akl block from the A (adjacency) matrix of G.

    Parameters
    ----------
    G : networx graph
        The whole RIN.
        
    k,l : int
        Subsystem indices.
    
    dimk, diml : int
        Dimension of subsystems k,l (number of vertices in k,l).
    
    idk, idl : int
        IDs of the nodes belonging to subsystems k,l 
        (the monomer attribute of G nodes).
    
    Returns
    -------
    Akl : numpy array (2d)
        The block containing edges of the SSI (PPI) between
        subsystems k,l.
    
    lbls : list
        List of node labels, first dimk labels for nodes in k,
        then diml labels for nodes in l.
    '''
    
    from collections import Counter
    
    Akl = np.zeros((dimk,diml))
    k_lbl = []
    l_lbl = []
    
    n = nx.number_of_nodes(G)
    
    # Convention: Akl[i,j] : i nodes in subsystem k
    #                        j nodes in subsystem l
    for j in range(n):
        
        j_mono = G.nodes[j]['monomer']
        
        if j_mono == idk:
            k_lbl.append(G.nodes[j]['label'])
        if j_mono == idl:
            l_lbl.append(G.nodes[j]['label'])
        
        for i in range(j):
            
            if G.has_edge(i,j):
                
                i_mono = G.nodes[i]['monomer']
                
                if (i_mono == idk and j_mono == idl) or (i_mono == idl and j_mono == idk):
                
                    count_up_to_i = Counter(MOLFRG[:i])
                    count_up_to_j = Counter(MOLFRG[:j])
                    #print('count_up_to_i = ',count_up_to_i)
                    #print('count_up_to_j = ',count_up_to_j)
                    
                    # the index shift going from A --> Akl
                    not_k_up_to_i = 0
                    not_l_up_to_i = 0
                    not_k_up_to_j = 0
                    not_l_up_to_j = 0
                    for key, val in count_up_to_i.items():
                        #print('up to i: key, val = ', key, val)
                        if key != idl:
                            not_l_up_to_i = not_l_up_to_i + val
                        if key != idk:
                            not_k_up_to_i = not_k_up_to_i + val
                
                    for key, val in count_up_to_j.items():
                        #print('up to j: key, val = ', key, val)
                        if key != idl:
                            not_l_up_to_j = not_l_up_to_j + val
                        if key != idk:
                            not_k_up_to_j = not_k_up_to_j + val
                    
                    if i_mono == idk and j_mono == idl:
                        ii = i - not_k_up_to_i
                        jj = j - not_l_up_to_j
                    if i_mono == idl and j_mono == idk:
                        ii = j - not_k_up_to_j
                        jj = i - not_l_up_to_i
                
                
                    
                    #print('i, j',  i, j, 'ii, jj', ii, jj)
                    #print(not_k_up_to_i, not_l_up_to_j, not_k_up_to_j, not_l_up_to_i, '|', MOLFRG[:i].count(idl), MOLFRG[:j].count(idk), MOLFRG[:j].count(idl), MOLFRG[:i].count(idk))
                    Akl[ii,jj] = G[i][j]['weight']
    
    
    return Akl, k_lbl, l_lbl

#-----------------------------------

def read_input_param(inp_file):
    '''
    Define parameters necessary to carry on. Not all parameters 
    have to be set in the inp_file - most have defaults. 
    Only these are mandatory:  
    
    case 1.     fmo_inp & fmo_out
                if you wish to read from GAMESS input/output files
    
    case 2.     PIE_csv_file
                If you wish to read from a comma separated value file
                If defined, this overrides case 1. But if neither 
                is given (case 1 or 2) we quit.
                In this case we must define SS_dict to define SSid.
    
    The inp_file format should follow the following rules:
        
        Everything after first '#' in line is considered comment.

        It is ok to have multiple "keyword=value" sets in one line, 
        if sets separated by ';'.
        e.g. Elim = -1.2; include_rep = False   is OK

        In lists and dicts, separate the elements by ','
        e.g. some_list=1, 100  is OK

        The GAMESS calculation should be either FMO/PIEDA or
        PA (partition analysis), so that we get also the
        energy terms E_tot, E_es, E_disp, G_solv

    Parameters
    ----------
    inp_file : str
        Contains the necessary & optional parameters. 
        Format for the input is in the provided template document.

    Returns
    -------
    param_dict : dict
        Dictionary holding the parameter values.
    '''
    
    param_dict = dict()
    
    # First set defaults, that can be overwritten vy values in the inp_file
    param_dict['fmo_inp'] = None
    param_dict['fmo_out'] = None
    param_dict['PIE_csv_file'] = None
    param_dict['node_label_file'] = None
    param_dict['out_f'] = 'svd_ppi_out.dat'
    param_dict['E_lim'] = -3.0  # kcal/mol
    param_dict['idw'] = None
    param_dict['include_rep'] = False
    param_dict['sort_CSV'] = False
    param_dict['print_nonzero_CSV'] = False
    param_dict['CSV_incl_wb'] = False
    param_dict['draw_PPI'] = False
    param_dict['show_click_plot'] = False
    param_dict['SEGMENTS_USE'] = False
    param_dict['auto_SS'] = True
    param_dict['water_network'] = False
    param_dict['PPI_draw_box'] = False
    param_dict['edge_labels_attr'] = 'edge_label'
    param_dict['pict_format'] = '.png'
    param_dict['quartile_type'] = 'nonzero'
    param_dict['SScolor'] = {0:'black', 1:'blue', 2:'red', 3:'green', 4:'orange', 5:'purple', 6:'olive'}
    param_dict['SScolor_rgb'] = {0:(0,0,0), 1:(102,204,255), 2:(255,128,128), 3:(128,255,191), 4:(255,212,128), 5:(234,128,255), 6:(179,179,0)}
    
    custom_SScolor = False
    custom_SScolor_rgb = False
    
    # Read the input file
    with open(inp_file) as inpfile:
        for i, line in enumerate(inpfile):
            if '#' in line:
                # A line containing comments - everything after first # is comment
                params, comnt = line.split('#', 1)
                #print(i, params, comnt)
            else:
                # A line without any comment
                params = line
                #print(i, params)
            
            # Identify recognized keywords in the params
            # Remove spaces & split, if multiple kaywords in one row
            parm_list = (params.replace(' ', '')).split(';')
            
            for itm in parm_list:
                
                # keyword & value pairs must be separated by '='
                
                if 'fmo_inp' in itm:
                    kwd, val = itm.split('=')
                    param_dict['fmo_inp'] = val.strip()
                
                if 'fmo_out' in itm:
                    kwd, val = itm.split('=')
                    param_dict['fmo_out'] = val.strip()
                
                if 'PIE_csv_file' in itm:
                    kwd, val = itm.split('=')
                    param_dict['PIE_csv_file'] = val.strip()
                
                if 'node_label_file' in itm:
                    kwd, val = itm.split('=')
                    param_dict['node_label_file'] = val.strip()
                
                if 'out_f' in itm:
                    kwd, val = itm.split('=')
                    param_dict['out_f'] = val.strip()
                
                if 'E_lim' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    param_dict['E_lim'] = float(val)
                
                if 'idw' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    param_dict['idw'] = int(val)
                
                if 'water_network' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['water_network'] = True
                    else:
                        param_dict['water_network'] = False
                
                if 'include_rep' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['include_rep'] = True
                    elif 'o' in val or 'O' in val:
                        param_dict['include_rep'] = 'only'
                    else:
                        param_dict['include_rep'] = False
                
                if 'sort_CSV' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['sort_CSV'] = True
                    else:
                        param_dict['sort_CSV'] = False
                
                if 'print_nonzero_CSV' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['print_nonzero_CSV'] = True
                    else:
                        param_dict['print_nonzero_CSV'] = False
                
                if 'CSV_incl_wb' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['CSV_incl_wb'] = True
                    else:
                        param_dict['CSV_incl_wb'] = False
                
                if 'draw_PPI' in itm:
                    kwd, val = itm.split('=')
                    #val.strip()
                    if 't' in val or 'T' in val:
                        param_dict['draw_PPI'] = True
                    else:
                        param_dict['draw_PPI'] = False
                
                if 'show_click_plot' in itm:
                    kwd, val = itm.split('=')
                    if 't' in val or 'T' in val:
                        param_dict['show_click_plot'] = True
                    else:
                        param_dict['show_click_plot'] = False
                
                if 'SS_dict' in itm:
                    param_dict['auto_SS'] = False
                
                if 'SScolor' in itm:
                    custom_SScolor = True
                
                if 'SScolor_rgb' in itm:
                    custom_SScolor_rgb = True
                    
                if 'SEGMENTS_USE' in itm:
                    kwd, val = itm.split('=')
                    if 't' in val or 'T' in val:
                        param_dict['SEGMENTS_USE'] = True
                    else:
                        param_dict['SEGMENTS_USE'] = False
                
                if 'PPI_draw_box' in itm:
                    kwd, val = itm.split('=')
                    if 't' in val or 'T' in val:
                        param_dict['PPI_draw_box'] = True
                    else:
                        param_dict['PPI_draw_box'] = False
                
                if 'edge_labels_attr' in itm:
                    kwd, val = itm.split('=')
                    if 'None' in val or 'none' in val or 'NONE' in val:
                        param_dict['edge_labels_attr'] = None
                    else:
                        param_dict['edge_labels_attr'] = str(val)
                
                if 'pict_format' in itm:
                    kwd, val = itm.split('=')
                    param_dict['pict_format'] = val.strip()
                
                if 'quartile_type' in itm:
                    kwd, val = itm.split('=')
                    param_dict['quartile_type'] = val.strip()
    
    inpfile.close()
    
    
    # Custom colour scheme for subsystems
    # If either of SScolor or SScolor_rgb changed, really other should change too.
    if custom_SScolor == True:
        import VS_various
        param_dict['SScolor'] = VS_various.read_dict_from_file(inp_file, dict_name='SScolor')
    if custom_SScolor_rgb == True:
        import VS_various
        param_dict['SScolor_rgb'] = VS_various.read_dict_from_file(inp_file, dict_name='SScolor_rgb')
    
    
    # Some matplotlib annoyance with versions...
    if param_dict['PPI_draw_box'] == False:     
        # Apparently, some older (?) matplotlib versions have trouble with the 
        # ax.spines[:].set_visible(False) method. This is used to remove the frame/box
        # arround the PPI graph. So we check and set/override PI_draw_box flag.
        import matplotlib
        mpl_vers = matplotlib.__version__
        if int(mpl_vers.split('.')[0]) >= 3 and int(mpl_vers.split('.')[1]) >= 5 :
            param_dict['PPI_draw_box'] = False
        else:
            param_dict['PPI_draw_box'] = True
            print('Matplotlib version:', mpl_vers, '=> we set PPI_draw_box = True')
            print('This is to avoid some trouble with ax.spines[:].set_visible(False) method\n')
    
    
    return param_dict

#-----------------------------------



#===================================
#-----------------------------------
#       P R O G R A M
#-----------------------------------
#===================================



# First thing - we check if all necessary modules are available

import_ok = True

try:
    import networkx as nx
    # https://networkx.org/documentation/stable/install.html
except BaseException as e:
    msg = '\n!!! Hopla, we get this message: ' + str(e) + '\n    You must install it to run this plugin. \n    Check: https://networkx.org/documentation/stable/install.html'
    print(msg)
    import_ok = False

try:
    import Read_FMO
    import VS_various
    import Graph_routines
except BaseException as e:
    msg = '\n!!! Hopla, we get this message: ' + str(e) + '\n    Seems like the modules are not recognised/found by python.\n  Where did you place them?'
    print(msg)
    import_ok = False

if import_ok == False:
    print('\nPlese fix modules. Exiting.')
    sys.exit()



#-----------------------------------
# The input file with certain parameters
parser = argparse.ArgumentParser(description='Define input file with necessary parameters.')
parser.add_argument('inp_file', metavar='some-file.csvinp', type=str, help='= name of the input file')
args = parser.parse_args()

inp_file = args.inp_file



#-----------------------------------
# Read required parameters & set defaults, 
# if values not specified.
param_dict = read_input_param(inp_file)
#print("param_dict =", param_dict)

# Decide if we're using GAMESS FMO output files or a PIE .csv table.
# Check if data compatible - user can mess up in several ways :). 
# Also, set priority.
use_PIE_csv = False
if param_dict['fmo_inp'] == None or param_dict['fmo_out'] == None:
    
    if param_dict['PIE_csv_file'] == None or param_dict['auto_SS'] == True:
        print('\n!!!\n fmo_inp and/or fmo_out not defined,\n so PIE_csv_file and SS_dict are mandatory,\n but are not defined -- needs a fix.\n Exiting.')
        sys.exit()

# If PIE_csv_file defined, it takes precedence over FMO files
if param_dict['fmo_inp'] != None and param_dict['fmo_out'] != None and param_dict['PIE_csv_file'] != None:
    print('\nPIE_csv_file was set - takes precedence over FMO files.\n')

# Check if we have both PIE_csv_file and SS_dict
if param_dict['PIE_csv_file'] != None and param_dict['auto_SS'] == True:
    print('\nPIE_csv_file was set, but no SS_dict,\nwhich is mandatory in such case -- needs a fix.\n Exiting.')
    sys.exit()

# Fianlly, if all is OK give green light
if param_dict['PIE_csv_file'] != None and param_dict['auto_SS'] == False:
    use_PIE_csv = True
    

# Output file
out_f = open(param_dict['out_f'], 'w')
# Print some header to out_f
print_header(out_f)



#-----------------------------------
# Current working directory
import os
res_dir = os.getcwd()



#-----------------------------------
# Read PIEs

if use_PIE_csv == False:
    
    # Read FMO output
    # to get E_ij values
    
    #NFRAG = Read_FMO.READ_NFRAG(param_dict['fmo_inp'])
    SEGMENTS_USE, NSEG, SEGDEF = Read_FMO.CHECK_SEGMENTS_USED(param_dict['fmo_out'])
    
    
    if SEGMENTS_USE == True and param_dict['SEGMENTS_USE'] == True:
        NFRAG = NSEG
    else:
        NFRAG = Read_FMO.READ_NFRAG(param_dict['fmo_inp'])
        SEGMENTS_USE = False
        if SEGMENTS_USE == False and param_dict['SEGMENTS_USE'] == True:
            print(" NOTICE: requested to use segments, but not available.\m Resorting to fragments.")
            
            
    E_tot, E_es, E_disp, G_solv = get_PIE(param_dict['fmo_out'], NFRAG, SEGMENTS_USE)

if use_PIE_csv == True:
    
    # PIEs from .csv file
    #PIE_csv_file = 'PIE_table.csv'
    NFRAG, E_tot, E_es, E_disp, G_solv = VS_various.read_PIE_from_csv(param_dict['PIE_csv_file'])



#-----------------------------------
# Read node attributes (name and subsystem membership)
# and get some subsystem stats. If we have use_PIE_csv = True
# it means, we should, at this point, have param_dict['auto_SS'] = False.
# Otherwise we should not have come this far.


# MOLFRG list
if param_dict['auto_SS'] == False:
    SS_dict = VS_various.read_dict_from_file(inp_file, dict_name='SS_dict')
    MOLFRG = MOLFRG_from_SS_dict(SS_dict)
else:
    MOLFRG = Read_FMO.READ_MOLFRG(param_dict['fmo_out'], NFRAG, SEGMENTS_USE)


# Node labels / frag_name
if use_PIE_csv == True:
    frag_name = VS_various.read_LABELS_from_csv(NFRAG, param_dict['node_label_file'])
else:
    frag_name, _, _, _ = Read_FMO.FMO_FRAG_STAT_GAMESS(param_dict['fmo_out'], NFRAG, SEGMENTS_USE = SEGMENTS_USE)


# Subsystems stats
SSid = sorted(list(set(MOLFRG)))
NSS = len(SSid)
SSsizes = dict()
for v in SSid:
    #SSsizes.append(MOLFRG.count(v))
    SSsizes[v] = MOLFRG.count(v)

print('\nWe recognize {0} subsystems with IDs: {1} and the numbers of nodes in each SS are {2}.\n'.format(NSS, SSid, SSsizes))
out_f.write('\nWe recognize {0} subsystems with IDs: {1} \nThe numbers of nodes in each SS are {2}.\n'.format(NSS, SSid, SSsizes))
if param_dict['auto_SS'] == False:
    print('Automated segmentation was set to False (SS_dict present in input). We print this as a recap:\nSS_dict =', SS_dict)
    print('MOLFRG =', MOLFRG)

if NSS == 1:
    print('Since we have just 1 subsystem, we quit - nihil facere qui.')
    sys.exit()
    


#-----------------------------------
# Construct G

G = nx.Graph()

# nodes
for i, name in enumerate(frag_name):
    G.add_node(i, label = name, monomer = MOLFRG[i])

# edges
for i in range(NFRAG):
    for j in range(i):
        E = E_tot[i,j]
        
        if E <= param_dict['E_lim'] and param_dict['include_rep'] != 'only':
            G.add_edge(i, j, weight=E)
        
        if E >= abs(param_dict['E_lim']) and param_dict['include_rep'] != False:
            G.add_edge(i, j, weight=E)



#-----------------------------------
# The matrix, or matrices, to be analysed
# (weighted adjacency matrix blocks of PPIs)

time_start = time.time()

# Iterate over all possible combinations of PPIs.
# That means over all pairs of subsystems k,l, where k<l
# (e.g 0,1; 1,2; etc.).

for l in range(NSS):
    for k in range(l):
        
        idk = SSid[k]
        idl = SSid[l]
        dimk = SSsizes[idk]
        diml = SSsizes[idl]
        #print('\nk, l, idk, idl, dimk, diml', k, l, idk, idl, dimk, diml)
        
        
        
        # Make & fill the k-l "adjacency" matrix.
        # This matrix is not (in general) square
        # and is of schape dimk*diml, i.e. the
        # rows correspond to nodes in SS k, and
        # columns to nodes in SS l.
        Akl_name = 'A_'+str(idk)+'_'+str(idl)
        Akl, k_lbl, l_lbl = make_Akl(G, k, l, dimk, diml, idk, idl)
        lbls = k_lbl+l_lbl
        
        
        # Test if Akl matrix contains non-zero elements.
        # If not, no sense to run SVD and other results analysis.
        # Set Akl_eval = True / False flag.
        if np.count_nonzero(Akl) > 0:
            print('\n' + Akl_name + ': has nonzero elements, carry on!')
            C_SV = asymmCSV(Akl)
            Akl_eval = True
        else:
            print('\n' + Akl_name + ': only zeros\n\tE_lim setting OK?')
            C_SV = [0.0 for _ in range(dimk+diml)]
            Akl_eval = False
        
        
        
        # Print some data & results
        out_f.write('\n\nAkl = {0}\n'.format(Akl_name))
        
        # Print the Akl matrix
        #np.savetxt(out_f, Akl)
        
        
        
        # TIE = total interaction enenrgy, or
        # sum of edge weights of node in graph.
        wg_sum_k = np.sum(Akl, axis=1)
        wg_sum_l = np.sum(Akl, axis=0)
        wg_sum = list(wg_sum_k) + list(wg_sum_l)
        
        
        
        # Print CSV results
        out_f.write('\n Singular value centrality\n')
        out_f.write('{0:>5}{1:>5}{2:>10}{3:>10}{4:>10}\n'.format('i', 'subs', 'node', 'wg_sum', 'C_SV'))
        if param_dict['sort_CSV'] == True:
            
            subs = [idk for _ in range(dimk)]
            tmp = [idl for _ in range(diml)]
            subs = subs + tmp
            
            sorted_lst = sorted(zip(C_SV, lbls, subs), reverse=True)
            
            for i, v in enumerate(sorted_lst):
                
                if param_dict['print_nonzero_CSV'] == True and v[0] < 0.000001:
                    pass
                else:
                    out_f.write('{0:>5}{1:>5}{2:>10}{3:>10}\n'.format(i, v[2], v[1], round(v[0],3)))
            
        else:
            
            for i, v in enumerate(C_SV):
                if param_dict['print_nonzero_CSV'] == True and v < 0.000001:
                    pass
                else:
                    if i >= dimk:
                        tmp = idl
                    else:
                        tmp = idk
                    out_f.write('{0:>5}{1:>5}{2:>10}{3:>10}{4:>10}\n'.format(i, tmp, lbls[i], round(wg_sum[i],3), round(v,3)))
        
        
        
        if Akl_eval == True:
            # Draw bar plot for CSV results
            # A list of colors by id (MOLFRG) values for nodes in subsystem k-l
            #color = ['blue' if i < dimk else 'red' for i in range(len(C_SV))]
            color = [param_dict['SScolor'][idk] if i < dimk else param_dict['SScolor'][idl] for i in range(len(C_SV))]
        
            bar_plot_fig_name = 'CSV_subsyst_'+str(idk)+'_'+str(idl)+param_dict['pict_format']
        
            draw_bar_plot_clickable(lbls, C_SV, color, bar_plot_fig_name, border=dimk, quartiles=param_dict['quartile_type'], show_click_plot=param_dict['show_click_plot'], ylabel=r'$C^{\mathrm{SV}}$')
        
        
            # A graph used for drawing of PPI.
            # Will include only nodes that do have k-l interactions, 
            # not all residues in k,l.
            if param_dict['draw_PPI'] == True:
                G_PPI_kl = make_GPPI_kl(G, idk, idl, E_es, G_solv, E_disp)
            
            # Draw PPI network
            if param_dict['draw_PPI'] == True and nx.is_empty(G_PPI_kl) == False:
                
                # Change .eps to .png to change figure format
                PPI_kl_file_name = '/'.join((res_dir, 'PPI_interface_'+str(idk)+'_'+str(idl)+param_dict['pict_format']))
                Graph_routines.PPI_draw2(G_PPI_kl, sort = 'ladder', monomer_attr = 'monomer', weight_attr = 'weight', labels_attr = 'label',
                                        edge_labels_attr = param_dict['edge_labels_attr'], Ene_attr = 'Ene', fact_attr = 'PIC', fact_crit = 0.5, 
                                        draw_box = param_dict['PPI_draw_box'], edge_labels_pos=0.7, PPI_fig_file = PPI_kl_file_name,
                                        RGB_A = param_dict['SScolor_rgb'][idk], RGB_B = param_dict['SScolor_rgb'][idl])
        
        
        
            # Akl statistics
            #topo_stat(Akl, C_SV)
        


#-----------------------------------
# Waters in water-mediated contacts
# a.k.a. structural waters / water bridges.

# First, check if idw setting compatible with SSid
if param_dict['idw'] != None:
    if param_dict['idw'] not in SSid:
        print('\nYour idw is not amongst the subsystem IDs.\nFix it. For now setting idw = None.')
        param_dict['idw'] = None



if param_dict['idw'] != None:

    water_nodes = []
    for n, mono in G.nodes(data='monomer'):
        if mono == param_dict['idw']:
            water_nodes.append(n)
            
    
    # Check if given water is a bridging water, if yess, collect data
    G_wSSI = nx.Graph()
    tmp2 = []
    for w in water_nodes:
        # Get SSid for each neighbour of given water
        tmp = [mono for _, mono in nx.ego_graph(G, w).nodes(data='monomer')]
        # Remove other water SSid
        ssids = [v for v in tmp if v != param_dict['idw']]
        # If at least 2 different monomers neighbour with this water
        # then add it to graph - it's a briging one.
        if len(set(ssids)) >= 2:
            tmp2.append(w)
            for i, j, E in G.edges(w, data='weight'):
                G_wSSI.add_edge(i, j, weight=E)
    
    # Waters - only bridging ones
    water_nodes = tmp2
    
    
    if param_dict['water_network'] == False:
        # It can happen that a bridging water will have interaction
        # also with another water (besides the bridging interactions).
        # It is optional to remove these from the overview.
        w_w_edges_to_be_removed = []
        for i, j in G_wSSI.edges():
            # remove edge if both waters
            if G.nodes[i]['monomer'] == G.nodes[j]['monomer']:
                w_w_edges_to_be_removed.append((i,j))
        for e in w_w_edges_to_be_removed:
            G_wSSI.remove_edge(e[0], e[1])
        
        # After removal of edges, some nodes (waters) may remain
        # as nodes without edges. We remove those.
        tmp = []
        for n in G_wSSI.nodes():
            if G_wSSI.degree[n] == 0:
                tmp.append(n)
        for n in tmp:
            G_wSSI.remove_node(n)
    
    
    #for i, j, E in G_wSSI.edges(data='weight'):
    #    print(i, G.nodes[i]['monomer'], G.nodes[i]['label'], E, j, G.nodes[j]['monomer'], G.nodes[j]['label'])
    
    print('\nA breakdown of water bridges is printed in', param_dict['out_f'])
    out_f.write('\nStructuraly important waters\nWater bridges:\n')
    for w in water_nodes:
        out_f.write('\n{0}, SSid = {1}, node = {2}\n'.format(G.nodes[w]['label'], G.nodes[w]['monomer'], w))
        G_wSSI.add_node(w, monomer = G.nodes[w]['monomer'])
        for i, j, E in nx.ego_graph(G_wSSI, w).edges(data='weight'):
            if i != w:
                n = i
            if j != w:
                n = j
                
            out_f.write('\t{0}, SSid = {1}, node = {2}, E_ij = {3}\n'.format(G.nodes[n]['label'], G.nodes[n]['monomer'], n, E))
            G_wSSI.add_node(n, monomer = G.nodes[n]['monomer'])
    
        

#-----------------------------------
# CSV that accounts for water bridge
# effects in residue centrality.

if param_dict['CSV_incl_wb'] == True and param_dict['idw'] != None:
    
    # Get list of water-bridget SSid combinations.
    # It's a list of tuples (i.e. SSid combinations), where one combination will 
    # contain all SSid a given water bridges. So a combination is "one per water".
    # Then we will use set to get only all unique combinations.
    wb_SSid_combinations = []
    for w in water_nodes:
        tmp = []
        for i, mono in nx.ego_graph(G_wSSI, w, center=False).nodes(data='monomer'):
            tmp.append(mono)
            #print(i, mono)
        wb_SSid_combinations.append(tuple(set(sorted(tmp))))
    
    wb_SSid_combinations = list(set(wb_SSid_combinations))
    
    
    out_f.write('\n\nCSV analysis accounting for water bridges.\nThe following subsystems do have water-mediated contact(s): {0}\n'.format(wb_SSid_combinations))
    print('The following subsystems do have water-mediated contact(s): {0}\n'.format(wb_SSid_combinations))
    
    
    
    # Run over k,l combinations (where neither k, nor l are the water SS)
    # and check, if the k,l combination has also some water bridges.
    # The water "SS index" will be 'w' and it's SSid will be idw.
    
    # Get w
    idw = param_dict['idw']
    for l in range(NSS):
        idl = SSid[l]
        if idl == idw:
            w = k
    
    for l in range(NSS):
        for k in range(l):
            
            idk = SSid[k]
            idl = SSid[l]
            
            if idk != idw and idl != idw:
                
                k_l_wb_exists = False
                for combo in wb_SSid_combinations:
                    if idk in combo and idl in combo:
                        k_l_wb_exists = True
                
                print('\nWater bridge between subsystems {0}, {1} exists: {2}'.format(idk, idl, k_l_wb_exists))
                
                if k_l_wb_exists == True:
                
                    dimk = SSsizes[idk]
                    diml = SSsizes[idl]
                    dimw = SSsizes[idw]
                    
                    # Remove edges for waters, that are not in bridges.
                    # We go about it the other way arround - make an empty
                    # copy of G and fill it with edges of those waters,
                    # that form bridges.
                                        
                    Gcopy = nx.create_empty_copy(G, with_data=True)
                    
                    for wat in water_nodes:
                        for i, j, E in G.edges(wat, data='weight'):
                            #print(i, j, E)
                            if G.nodes()[i]['monomer'] == idw and G.nodes()[j]['monomer'] == idw:
                                # skip water--water edges
                                pass
                            else:
                                Gcopy.add_edge(i, j, weight=E)
                    
                    #for i, j, E in Gcopy.edges.data():
                    #    print(i, j, E)
                    
                    
                    
                    # Get block matrices (use Gcopy for blocks with water)
                    Akl, k_lbl, l_lbl = make_Akl(G, k, l, dimk, diml, idk, idl)
                    Akw, k_lbl, w_lbl = make_Akl(Gcopy, k, w, dimk, dimw, idk, idw)
                    Alw, l_lbl, w_lbl = make_Akl(Gcopy, l, w, diml, dimw, idl, idw)
                    
                    
                    
                    # Make the big matrix - the adjacency matrix
                    # with blocks of edges for the interactions:
                    # empty blocks: k-k , l-l, w-w
                    # edges in blocks: k-l (l-k), k-w (w-k), l-w (w-l).
                    Aklw = np.zeros((dimk+diml+dimw, dimk+diml+dimw))
                    Aklw_name = 'A_'+str(idk)+'_'+str(idl)+'_'+str(idw)
                    
                    # Fill in values to Aklw from Akl, Akw, Alw bloks.
                    # Make Aklw also symmetric.
                    
                    # k,l block(s)
                    for i in range(dimk):
                        for j in range(diml):
                            Aklw[i, dimk+j] = Akl[i,j]
                            Aklw[dimk+j, i] = Akl[i,j]
                    
                    # k,w block(s)
                    for i in range(dimk):
                        for j in range(dimw):
                            Aklw[i, dimk+diml+j] = Akw[i,j]
                            Aklw[dimk+diml+j, i] = Akw[i,j]
                    
                    # l,w block(s)
                    for i in range(diml):
                        for j in range(dimw):
                            Aklw[dimk+i, dimk+diml+j] = Alw[i,j]
                            Aklw[dimk+diml+j, dimk+i] = Alw[i,j]
                    
                    
                    lbls = k_lbl+l_lbl+w_lbl
                    
                    
                    
                    # Test if Akl matrix contains non-zero elements.
                    # If not, no sense to run SVD and other results analysis.
                    # Set Aklw_eval = True / False flag.
                    if np.count_nonzero(Akl) > 0:
                        print('\n' + Aklw_name + ': has nonzero elements, carry on!')
                        C_SV = symmCSV(Aklw)
                        Aklw_eval = True
                    else:
                        print('\n' + Aklw_name + ': only zeros\n\tE_lim setting OK?')
                        C_SV = [0.0 for _ in range(dimk+diml+dimw)]
                        Aklw_eval = False
                    
                    
                    
                    # Print some data & results
                    out_f.write('\n\nAklw = {0}\n'.format(Aklw_name))
                    
                    # Print the Aklw matrix
                    #np.savetxt(out_f, Aklw)
                    
                    
                    
                    # Count No. of water bridges of non-water residues.
                    wb_count_per_res = [0 for _ in range(dimk+diml+dimw)]
                    for i in range(dimk):
                        wb_count_per_res[i] = np.count_nonzero(Akw[i,:])
                    for i in range(diml):
                        wb_count_per_res[dimk+i] = np.count_nonzero(Alw[i,:])
                    
                    
                    
                    # TIE = total interaction enenrgy, or
                    # sum of edge weights of node in graph.
                    wg_sum = np.sum(Aklw, axis=1)
                    
                    
                    # Print CSV results
                    
                    # Subsystem id
                    subs = [idk for _ in range(dimk)]
                    tmp = [idl for _ in range(diml)]
                    tmp2 = [idw for _ in range(dimw)]
                    subs = subs + tmp + tmp2
                    
                    out_f.write('\n Singular value centrality (incl. water bridges)\n')
                    out_f.write('{0:>5}{1:>5}{2:>10}{3:>5}{4:>10}{5:>10}\n'.format('i', 'subs', 'node', '#wb', 'wg_sum','C_SV'))
                    if param_dict['sort_CSV'] == True:
                        
                        sorted_lst = sorted(zip(C_SV, lbls, subs, wb_count_per_res, wg_sum), reverse=True)
                        for i, v in enumerate(sorted_lst):
                            if param_dict['print_nonzero_CSV'] == True and v[0] < 0.000001:
                                pass
                            else:
                                out_f.write('{0:>5}{1:>5}{2:>10}{3:>5}{4:>10}{5:>10}\n'.format(i, v[2], v[1], v[3], round(v[4],3), round(v[0],3)))
                        
                    else:
                        
                        for i, v in enumerate(C_SV):
                            if param_dict['print_nonzero_CSV'] == True and v < 0.000001:
                                pass
                            else:
                                out_f.write('{0:>5}{1:>5}{2:>10}{3:>5}{4:>10}{5:>10}\n'.format(i, subs[i], lbls[i], wb_count_per_res[i], round(wg_sum[i], 3),round(v,3)))
                    
                    
                                        
                    if Akl_eval == True:
                        # Draw bar plot for CSV results
                        # A list of colors by id (MOLFRG) values for nodes in subsystem k-l
                        #color = ['blue' if i < dimk else 'red' for i in range(len(C_SV))]
                        color = [param_dict['SScolor'][i] for i in subs]
                    
                        bar_plot_fig_name = 'CSV_subsyst_'+str(idk)+'_'+str(idl)+'_'+str(idw)+param_dict['pict_format']
                    
                        draw_bar_plot_clickable(lbls, C_SV, color, bar_plot_fig_name, border=(dimk, dimk+diml), quartiles=param_dict['quartile_type'], show_click_plot=param_dict['show_click_plot'], ylabel=r'$C^{\mathrm{SV}}$')
                        
                        
                        
                        # Draw PPI network
                        if param_dict['draw_PPI'] == True:
                            # This next part is necessary to draw the tripartite PPIs.
                        
                            # Start from an deep copy of G, remove all edges to water
                            # and update the graph with edges from the existing graph
                            # Gcopy - has only edges for bridging waters. We use nx.compose for this.
                        
                            # 1. Deepcopy G
                            import copy
                            Gtmp = copy.deepcopy(G)
                        
                            # 2. Remove water edges
                            for node, attr in Gtmp.nodes(data=True):
                                if attr['monomer'] == idw:
                                    Gtmp.remove_edges_from(list(Gtmp.edges(node)))
                        
                            # 3. compose Gtmp with Gcopy
                            G2 = nx.compose(Gtmp, Gcopy)
                              
                            # A graph used for drawing of PPI.
                            # Will include only nodes that do have k-l-w interactions, 
                            # not all residues in k,l,w.
                            G_PPI_klw = make_GPPI_klw(G2, idk, idl, idw, E_es, G_solv, E_disp)
                        
                            if nx.is_empty(G_PPI_klw) == False:
                            
                                PPI_klw_file_name = '/'.join((res_dir, 'PPI_interface_'+str(idk)+'_'+str(idl)+'_'+str(idw)+param_dict['pict_format']))
                                Graph_routines.PPI_draw2(G_PPI_klw, sort = 'ladder_T', monomer_attr = 'monomer', weight_attr = 'weight', labels_attr = 'label',
                                                         edge_labels_attr = param_dict['edge_labels_attr'], Ene_attr = 'Ene', fact_attr = 'PIC', fact_crit = 0.5, 
                                                         draw_box = param_dict['PPI_draw_box'], edge_labels_pos=0.7, PPI_fig_file = PPI_klw_file_name,
                                                         RGB_A = param_dict['SScolor_rgb'][idk], RGB_B = param_dict['SScolor_rgb'][idl], RGB_C = param_dict['SScolor_rgb'][idw])
                    
                        
                            # Draw also the same G_PPI_klw, but only draw the water bridges
                            for i, j in G_PPI_klw.edges():
                                if G_PPI_klw.nodes()[i]['monomer'] != idw and G_PPI_klw.nodes()[j]['monomer'] != idw:
                                    G_PPI_klw.remove_edge(i, j)
                            
                            if nx.is_empty(G_PPI_klw) == False:
                            
                                PPI_klw_file_name = '/'.join((res_dir, 'PPI_interface_('+str(idk)+'_'+str(idl)+')_'+str(idw)+param_dict['pict_format']))
                                Graph_routines.PPI_draw2(G_PPI_klw, sort = 'ladder_T', monomer_attr = 'monomer', weight_attr = 'weight', labels_attr = 'label',
                                                         edge_labels_attr = param_dict['edge_labels_attr'], Ene_attr = 'Ene', fact_attr = 'PIC', fact_crit = 0.5, 
                                                         draw_box = param_dict['PPI_draw_box'], edge_labels_pos=0.7, PPI_fig_file = PPI_klw_file_name,
                                                         RGB_A = param_dict['SScolor_rgb'][idk], RGB_B = param_dict['SScolor_rgb'][idl], RGB_C = param_dict['SScolor_rgb'][idw])
                    

    


#-----------------------------------
out_f.close()


time_finish = time.time()
print("\nTotal run time (seconds) =", time_finish - time_start)