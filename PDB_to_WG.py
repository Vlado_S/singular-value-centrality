__author__    = "Vladimir Sladek"
__email__     = "sladek.vladimir@savba.sk"
__copyright__ = """ Copyright since 2024

                    This is free software: you can redistribute it and/or modify
                    it under the terms of the GNU General Public License as published by
                    the Free Software Foundation, either version 3 of the License, or
                    (at your option) any later version.
                    
                    This code is distributed in the hope that it will be useful,
                    but WITHOUT ANY WARRANTY; without even the implied warranty of
                    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
                    GNU General Public License for more details.
                    
                    GNU General Public License: <https://www.gnu.org/licenses/>.
                """



#-----------------------------------
#    G L O B A L  I M P O R T S
#-----------------------------------



import numpy as np
#import networkx as nx
#import time
import argparse
#import sys
#print(sys.path)
#print(sys.executable)









#-----------------------------------
#       F U N C T I O N S
#-----------------------------------

def compose_WG_csv_file(csv_file, dist_matrix, wg_type):
    '''
    Print the weights to a .csv type file for further use.

    Parameters
    ----------
    csv_file : str
        The .csv file where i, j, weight will be printed.
    
    dist_matrix : numpy 2d array
        Contains the res-res distances.
    
    wg_type : TYPE
        Defines whether to use the distance directly or the inverse of the 
        distance (if 'i' in the name):
        CA, CM, CMS, or iCA, iCM, iCMS.

    '''
    out_f = open(csv_file, 'w')
    out_f.write('# {0}\n'.format(wg_type))
    
    for i in range(dist_matrix.shape[0]):
        for j in range(i):
            
            if 'i' in wg_type:
                val = 1/dist_matrix[i,j]
            else:
                val = dist_matrix[i,j]
            
            out_f.write('{0:>5},{1:>5},{2:>10}\n'.format(i, j, round(val,5)))
    
    out_f.close()
    
    return

#-----------------------------------









#===================================
#-----------------------------------
#       P R O G R A M
#-----------------------------------
#===================================



#-----------------------------------
# The input file with certain parameters
parser = argparse.ArgumentParser(description='Generates a .csv file suitable for SVD_centrality.py.', epilog='Try again you must, young Skywalker ;)')
parser.add_argument('pdb_file', metavar='structure.pdb', type=str, help=' Name of the .pdb file.')
parser.add_argument('wg_type', metavar='wg_type', type=str, choices=['CA', 'CM', 'CMS', 'iCA', 'iCM', 'iCMS'], help=' Type of weight: CA, CM, CMS. \n Distance between CA atoms, centre of mass (CM) or centre of mass of side chains (CMS).\n Use the inverse of the distance if iCA (iCM, iCMS). \nFor non amino acid res all atoms are considered in CM and first atom if CA selected.')
parser.add_argument('output_name', metavar='output_name', type=str, help=' Name of the output_X.cvs files with (1) the edge weights; (2) node labels; (3) subsystem dictionary.')
args = parser.parse_args()

pdb_file = args.pdb_file
wg_type = args.wg_type
output_name = args.output_name


#-----------------------------------
# Import the PDB module of biopython
# https://biopython.org/wiki/The_Biopython_Structural_Bioinformatics_FAQ
# The star import is not desirable, as spyder does not automatically 
# recognize function names and annoying warning ared displayed (although functionality is OK)
#from Bio.PDB import *
import Bio.PDB

# Parser that reads PDB structure
biopypars = Bio.PDB.PDBParser()



#-----------------------------------
# Read the structure .pdb
try:
    structure = biopypars.get_structure('struct', pdb_file)
    
    atoms = list(structure.get_atoms())
    residues = list(structure.get_residues())
    NATOM = len(atoms)
    NRES = len(residues)
    NAA = 0
    for res in residues:
        if Bio.PDB.is_aa(res) == True:
            NAA = NAA + 1
        else:
            pass
            #print(res.get_resname(), res.get_id()[1], res.get_parent().id)
    print('\nRead ', NATOM, 'atom coordinates.')
    print('Total of ', NRES, 'residues, of which', NAA, ' amino acids.')
    
    structure_finish = True
except Exception as e:
    structure_finish = False
    print(e)



#-----------------------------------
# Get the residue distance matrix (lower triangular)

import Centre_of_Mass

# Distance type is similar as weight type, but the 'i' is irrelevant now.
dist_type = wg_type.replace('i','')

dist_matrix = Centre_of_Mass.distance_matrix_biopy(structure, dist_type=dist_type, model_id = 0)



#-----------------------------------
# Compose the .csv file with proper/desired weights

compose_WG_csv_file(output_name+'_WG.csv', dist_matrix, wg_type)



#-----------------------------------
# Compose the .csv file with node labels

out_f = open(output_name+'_LBL.csv', 'w')

for i, res in enumerate(residues):
    #print(i, res.resname+str(res.id[1]))
    out_f.write('{0:>5},{1:>10}\n'.format(i, res.resname+str(res.id[1])))

out_f.close()



#-----------------------------------
# Compose the .dict file with SS_dict
# Group by chain IDs.

res_ch_ids = []

for i, res in enumerate(residues):
    #print(i, res.resname+str(res.id[1]), res.get_parent().id)
    res_ch_ids.append(res.get_parent().id)

# Chain id (ordered/sorted set of unique IDs)
CHids = sorted(list(set(res_ch_ids)))
#print(CHids)

# A dictionary containing CHid as key, and a list of
# all residue indices (not res.id) in given chain.
CH_dict = {}
for chid in CHids:
    
    tmp = []
    for i, rchid in enumerate(res_ch_ids):
        if rchid == chid:
            tmp.append(i)
        
    CH_dict[chid] = tmp

#print(CH_dict)

# Write SS_dict to file - like CH_dict, but with index intervals.
# The SS_dict is actually not constructed, only wtitten to file!

out_f = open(output_name+'_SS.dict', 'w')
out_f.write('SS_dict = {\n')

NCH = len(CH_dict)

print('\nIdentified subsystems:*')
for i, (k, v) in enumerate(CH_dict.items()):
    print('chain', k, '--> SSid', i+1)
    
    # Identify index intervals
    tmp = []
    jj = 0
    for j in range(len(v)-1):
        #print(j, v[j], v[j+1]-v[j])
        if v[j+1]-v[j] != 1:
            tmp.append((v[jj],v[j]))
            jj = j+1

    tmp.append((v[jj], v[-1]))
    
    for itm in tmp:
        if itm[0] != itm[1]:
            print('\t{0}: residues {1}-{2}'.format(i+1, itm[0], itm[1]))
            if i < NCH - 1:
                out_f.write('{0}: \'{1}-{2}\',\n'.format(i+1, itm[0], itm[1]))
            else:
                out_f.write('{0}: \'{1}-{2}\'\n'.format(i+1, itm[0], itm[1]))
        else:
            print('\t{0}: residues {1}'.format(i+1, itm[0]))
            if i < NCH - 1:
                out_f.write('{0}: \'{1}\',\n'.format(i+1, itm[0]))
            else:
                out_f.write('{0}: \'{1}\'\n'.format(i+1, itm[0]))
    
print('   * If you had discontinuous order of res in chain,\n     then due to the way Bio.PDB iterates over the\n     structure they will be grouped together.')
out_f.write('}')
out_f.close()



#-----------------------------------
print('\nData in:\n   weights: {0}\n   labels: {1}\n   subs. dict: {2}'.format(output_name+'_WG.csv', output_name+'_LBL.csv', output_name+'_SS.dict'))



####################################
'''
#    0 1 2 3 4 5 6  7  8
l = [1,2,3,5,6,7,8,9,11,13]
ldiff = np.diff(l)
print(ldiff)

tmp = []
ii = 0
for i in range(len(l)-1):
    print(i, l[i], l[i+1]-l[i])
    if l[i+1]-l[i] != 1:
        tmp.append((l[ii],l[i]))
        ii = i+1

tmp.append((l[ii], l[-1]))
print(tmp)

L = []
for i, v in enumerate(tmp):
    if v[0] != v[1]:
        print('{0}: \'{1}-{2}\''.format(i, v[0], v[1]))
        L.append((v[0], v[1]))
    else:
        print('{0}: \'{1}\''.format(i, v[0]))
        L.append(v[0])

print('L =', L)
'''  





















